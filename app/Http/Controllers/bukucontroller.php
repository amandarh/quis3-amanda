<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class buku extends Controller
{
    function index()
    {
        return view('home');
    }
    public function buku()
    {
        // mengambil data dari table pegawai
        $buku = DB::table('buku')->get();

        // mengirim data buku ke view index
        return view('buku', ['buku' => $buku]);
    }
}

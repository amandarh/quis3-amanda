<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'bukucontroller@index');
Route::get('/buku', "bukucontroller@buku");
Route::get('/buku/edit/{id}', "bukucontroller@edit");
Route::get('/buku/show/{id}', "bukucontroller@show");
Route::get('/buku/create', "bukucontroller@create");
Route::post('/buku/store', "bukucontroller@store");
Route::post('/buku/update/{id}', "bukucontroller@update");
Route::get('/buku/delete/{id}', "bukucontroller@destroy");
